import React, { Component } from 'react'
import { ServerStyleSheet } from 'styled-components'
import { reloadRoutes } from 'react-static/node'
import chokidar from 'chokidar'
import glob from 'glob'

var asciidoctor = require('asciidoctor.js')();

// Hot-load blog/notes
chokidar.watch('./content').on('all', () => reloadRoutes())


export default {
	getSiteData: () => ({
		title: "Davids blog",
	}),
	getRoutes: async () => {

		const urlify = string => {
			return string;
		}

		// Get relevant fields from asciidoc
		const make_post = adoc => ({
			title: adoc.getDocumentTitle(),
			author: adoc.getAuthor(),
			date: adoc.getRevisionDate(),
			url: urlify(adoc.getDocumentTitle()),
			html: adoc.convert()
		})

		const get_posts = dir => {
			let files = glob.sync(dir);
			let adocs = files.map(file => asciidoctor.loadFile(file));
			return adocs.map(doc => make_post(doc));
		}

		const blog_posts = get_posts("content/blog/*.adoc");
		const notes = get_posts("content/notes/*.adoc");

		return [
			{
				path: '/',
				component: 'src/pages/Home',
			},
			{
				path: '/blog',
				component: 'src/pages/List',
				getData: async () => ({ 
					entries: blog_posts,
					base_url: "/blog" 
				}),
				children: blog_posts.map(post => ({
					path: post.url,
					component: 'src/pages/Entry',
					getData: async () => ({
						entry: post,
					}),
				})),
			},
			{
				path: "/notes",
				component: "src/pages/List",
				getData: async () => ({
					 entries: notes,
					 base_url: "/notes"
				}),
				children: notes.map(note => ({
					path: note.url,
					component: 'src/pages/Entry',
					getData: async () => ({
						entry: note,
					}),
				})),

			},
			{
				is404: true,
				component: 'src/pages/404',
			},
		]
	},
	renderToHtml: (render, Comp, meta) => {
		const sheet = new ServerStyleSheet()
		const html = render(sheet.collectStyles(<Comp />))
		meta.styleTags = sheet.getStyleElement()
		return html
	},
	Document: class CustomHtml extends Component {
		render () {
			const {
				Html, Head, Body, children, renderMeta,
			} = this.props

			return (
				<Html>
					<Head>
						<meta charSet="UTF-8" />
						<meta name="viewport" content="width=device-width, initial-scale=1" />
						{renderMeta.styleTags}
					</Head>
					<Body>{children}</Body>
				</Html>
			)
		}
	},
}
