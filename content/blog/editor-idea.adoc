= Idea: Hybrid WYSIWYG/Plaintext Editor
David Wood <dwood023@live.co.uk>
19 Aug 2018

// Holy shit I spent an hour finding out that DOUBLE BACKTICKS are needed to avoid this getting interfered with inline in a source block.
// I'm not even sure why it was fixed in the end, but I can at least use this as a working and complex example of substitution
:cursor: [.nopad.blinking]``|``

Dude, hear me out **HITS BLUNT**. Plaintext formats are basically good for _writing_, but not for reading, right?footnote:[On the other hand, WYSIWYG editors (eg. MS Word) let you see the formatting as you type, but it's fiddly to produce in the first place.]

You only _edit_ the line your cursor is on, and you're most interested in _reading_ lines *surrounding* the one being written.

Here's the idea:

The whole document is nicely formatted _in the editor_, except for the line your cursor is on. Any line the cursor moves to reveals its actual asciidoc syntax.

== Pros/Cons

[.pros]
* Easily read most of the document as you type
* Preview output _without_ side-by-side editor/preview windows

[.cons]
* Preview text is more compact, so when expanding, everything will shift right?
* Some asciidoc features affect the output, but aren't visible themselves in the preview
* Implement how? How do you even know what features of the output correspond to which in the source material?

=== Con #1  --  Content shift

Let's say you see the following formatted line:

[source, subs="quotes"]
~Check~ this *WACKY* ^SHIT^ _yoooooo_

Uh oh, there's a https://youtu.be/yD0_XnPlpf4?t=12s[wordy dird in there.] Better move the cursor there and edit that out.footnote:[This is a PG-13 blog so fuck off with your profanity.]

[source, subs="attributes, normal"]
\~Check~ this \*WACKY* ^{cursor}SHIT^ \_yoooooo_


Woah, there's the markup - but everything had to move to fit it in! ლಠ益ಠ)ლ

The cursor is still where you wanted it (relative to the content), but you can't really tell where everything will be until you actually move the cursor there, which could slow someone down significantly.

=== Con #2  --  Invisible output == Un-editable input

The cursor reveals markup and enables editing, so if you can't see it in the output, you can't move the cursor there, and it can't be edited. For example, block metadata:

[source, subs="normal"]
----
[quote]
What doth life?
----

[quote]
What doth life?

What would you click on to turn this *quote* into something else? Unless related metadata is also unhidden when the line gains focus, but how..?

The underlying mis-assumption here is that markup maps line-by-line to its output format.
This is true for basic inline stuff like bold and italics, but not for other important features.
Here's some examples:

* Blocks -> [this is above it's related line]
* Footnotes -> \footnote:[this shows up at the bottom]
* Variables -> \{could_be_defined_anywhere}
* Tables -> |=== (This isn't even just text anymore)

=== Con #3  --  Mapping effect to cause

I guess this design is literally backwards in nature - you're interacting with the input *through* the output. Therefore, to edit the source document, you need to map everything in the output to every feature of markup that influenced it's formatting or content.

Once this mapping is done, you can show *all* of the relevant source lines once a line gains focus in the preview. footnote:[For example, a line containing a variable can provide access to that variable's definition, which would otherwise not be visible or editable in the output document.]

== Evaluation

There's two categories of problem here:

* Whether it *can* be made
  .. Is the mapping thing at all feasible?
  .. How is the basic editor stuff handled?
    ... ContentEditable shenanigans
    ... VIM bindings from where?footnote:[If it doesn't have this, I wouldn't use it anyway]
* Whether it *should* be made
  .. Is the shifting content just going to be annoying to use?
  .. Isn't the advantage of plaintext that everything is clearly *what it is*, with no hidden bullshit screwing you over?

== Conclusion

Uhh, I've almost talked myself out of it already. It's a neat idea in some ways, and it could be fun making a software representation of the connection between asciidoctor's input and output, this could be useful for making higher-level assistance features.

However, in the process of writing this post I've kind of come to like just writing plain asciidoc in atom with some nice syntax highlighting. So probably it's not worth the trouble.

It's amazing that for once, I've uncovered the real trash beneath one of my ideas even _before_ I've wasted hundreds of hours [strike]``programming`` confused looking up basic shit on google.
