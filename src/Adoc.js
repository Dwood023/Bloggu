import {text_color, yellow, green, red} from "./constants";
import styled from "styled-components";
import React from "react";
import tinycolor from "tinycolor2";

const quote_color = tinycolor(yellow).darken(15).desaturate(25).toString();
const attrib_color = tinycolor(quote_color).darken(10).toString();

export const hash_before = `
    content: "# ";
    color: ${yellow};
    position: relative;
    left: -1rem;
    margin-right: -1rem;
`

const Adoc = styled.div`
	.quoteblock {
		padding: 5px 0;
		color: ${quote_color};
		font-weight: 200;

		blockquote::before {
			content: "\\201c";
			float: left;
			font-size: 2.75em;
			font-weight: bold;
			line-height: .6em;
			margin-left: -.6em;
			margin-right: .3em;
			text-shadow: 0 1px 2px rgba(0,0,0,.1);
		}

		.attribution {
            color: ${attrib_color};
			text-align: right;
			font-size: 85%;
		}
	}

	.dlist {
		padding: 5px 0;
		font-size: 90%;
		dt {
			font-weight: 500;
		}
		dd {
		}
	}

	strong {
		font-weight: bold;
	}

	pre {
		white-space: pre-wrap;
	}
	h2 {
		padding: 1.5rem 0;
	}
	h3, h4, h5 {
		padding: 1rem 0;
	}
	h2, h3, h4, h5, h6 {
		&::before {
            ${hash_before}
		}
	}
	#footnotes {
		padding-top: 2rem;
	}
	.footnote a {
		color: ${yellow};
	}

	/* Bibliography refs aren't class-tagged, unfortunately*/
	.paragraph a[href^="#"]:not(.footnote) {
		vertical-align: super;
		font-size: smaller;
	}
	/* TODO don't repeat yourself w/ pros/cons */
	.pros {
		li {
			list-style-type: none;
			color: ${green};
			&:before {
				content: "+";
				float: left;
				margin-right: .3em;
			}
		}
	}

	.cons {
		li {
			list-style-type: none;
			color: ${red};
			&:before {
				content: "-";
				float: left;
				margin-right: .3em;
			}
		}
	}

	.nopad {
		letter-spacing: -.2rem;
		position: relative;
		left: -0.1rem;
	}
	.blinking {
		animation: 0.4s infinite alternate blink;
	}
	@keyframes blink {
		from {
			opacity: 1;
		}
		to {
			opacity: 0;
		}
	}
	.strike {
		text-decoration: line-through;
	}

	.sidenote {
		font-size: 11px;
		color: ${tinycolor(text_color).darken(15).toString()};
		display: block;
		padding: 20px;

		@media only screen and (min-width: 1150px) {
			float: right;
			position: relative;
			top: -1rem;
			left: 250px;
			margin-left: -250px;
			width: 200px;
			clear: right;
			padding-top: 0;
		}
	}
`;

export default ({children}) => (
    <Adoc>
        {children}
    </Adoc>
)