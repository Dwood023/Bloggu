import React from 'react'
import { withSiteData } from 'react-static'
import { hot } from 'react-hot-loader'

export default hot(module)(withSiteData(() => (
	<div>
		<p>If you can find any meaning at all in what you read here, my therapist would like a word with you.</p>
		<a href="mailto:Dwood023@live.co.uk">Mail me son</a>
	</div>
)))
