import React from 'react'
import { withRouteData } from 'react-static'
import convert from 'htmr';
import styled from "styled-components";
import { hot } from 'react-hot-loader'
import cheerio from "cheerio";

import { yellow } from "../constants";
import ADoc from "../Adoc";
import ToC from "../components/ToC";

/* AsciiDoc specific styling */

const Metadata = styled.div`
	margin-bottom: 2rem;
`;
const Title = styled.h1`
	font-weight: 500;
	font-size: 150%;
	color: ${yellow};
	margin-bottom: 0;
`;
const Author = styled.time`
	font-weight: bold;
	padding-right: 15px;
	letter-spacing: 1px;
`;

const Entry = ({ entry }) => {

	const $ = cheerio.load(entry.html);

	const headings = $("h2, h3, h4, h5, h6").map((i, heading) => (
		{
			title: heading.firstChild.data,
			id: heading.attribs.id,
			level: parseInt(heading.name[1]) // Just want X from hX
		}
	)).toArray();
	const has_headings = headings.length != 0;
	
	return (
		<ADoc>
			<Metadata>
				<Title>{entry.title}</Title>
				<Author>{entry.author.toUpperCase()}</Author>
				{entry.date}
			</Metadata>
			{ // Conditionally render ToC
				has_headings && <ToC headings={headings}/> 
			}
			{convert(entry.html)}
		</ADoc>
	)
}

export default hot(module)(withRouteData(Entry))
