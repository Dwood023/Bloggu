import React from 'react'
import { Link } from 'react-static'
import styled from 'styled-components'
import { hot } from 'react-hot-loader'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faInfinity } from '@fortawesome/free-solid-svg-icons'

const Nav = styled.div`
	padding: 45px 15px;

	a {
		margin-right: 20px;
		padding-right: 20px;
		border-right: 1px dotted #ffc456;
		vertical-align: middle;
		font-weight: 300;
        font-size: 90%;
	}
	a:last-child {
		border-right: 0;
    }
    #title {
        font-size: 150%;
        color: #c9cacc;
        letter-spacing: 1.5px;
        margin-bottom: 0;
    }
`;

const Icon = styled(FontAwesomeIcon)`
	float: left;
	margin-right: 15px;
    padding: 15px 0;
	&:hover {
        color: rgb(255, 196, 86);
	}
`;


export default hot(module)(() => (
    <Nav>
        <Icon icon={faInfinity} size="3x" />
        <h1 id="title">Deadly Bloggu</h1>
        <Link exact to="/">Home</Link>
        <Link to="/blog">Blog</Link>
        <Link to="/notes">Notes</Link>
    </Nav>
))