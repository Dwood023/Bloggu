import React from "react";
import styled from "styled-components";
import { hot } from 'react-hot-loader'
import tinycolor from "tinycolor2";

import {hash_before} from "../Adoc";
import {yellow, text_color} from "../constants";

const Panel = styled.div`
	@media only screen and (min-width: 1150px) {
		position: fixed;
		left: 0px;
		padding: 20px;
		width: 200px
	}
    padding-left: 5px;
    ul {
        padding-left: 20px;
    }
    a[href$="${props => props.ActiveLink}"] {
        color: ${yellow};
    }
`
const Entry = styled.li`
    font-size: ${props => 13 - (props.level / 2)}px;

    list-style-type: none;
    padding-top: 3px;
    a {
        text-decoration: none;
        color: ${
            props => (
                tinycolor(text_color)
                    .darken((props.level - 2) * 10)
                    .toString()
            )
        };
        &:before {
            ${hash_before};
        }
    }
`;
const get_entry = heading => (
    <Entry key={heading.id} level={heading.level}>
        <a href={`#${heading.id}`}>
            {heading.title}
        </a>
    </Entry>
);

const ToCTitle = styled.p`
    font-weight: 500;
`;

/*
[2, 3, 4, 4, 3, 2, 3] // Input
[2, [3, [4, 4], 3], 2, [3]] //Output

[2, 3, 4, 5, 2, 2, 3] // Input
[2, [3, [4, [5]]], 2, 2, [3]] //Output
*/
const unflatten = headings => {
    
    let output = [headings[0]]; // Guaranteed first element
    for (let i = 1 ; i < headings.length ;) {
        const 
            current_lvl = headings[i].level,
            prev_lvl = headings[i-1].level;

        // If nesting level increases
        if (current_lvl > prev_lvl) {
            const after = headings.slice(i);

            const next_higher = after.findIndex(e => e.level < current_lvl);
            const higher_not_found = next_higher === -1;

            const nest_bounds = (higher_not_found) ? after.length : next_higher
            const nested_array = after.slice(0, nest_bounds);

            output.push(unflatten(nested_array));
            i += nest_bounds;
        }
        else {
            output.push(headings[i]);
            i++;
        }
    }
    return output;
}

const get_list = headings => {
    let list = [];

    for (let heading of headings) {
        if (Array.isArray(heading)) list.push(
            get_list(heading)
        )
        else list.push(
            get_entry(heading)
        )
    }
    return <ul key={new Date()}>{list}</ul>
};


class ToC extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nearest_anchor: this.props.headings[0].id
        };
        this.onScroll = this.onScroll.bind(this);
    }
    componentDidMount() {
        window.addEventListener("scroll", this.onScroll);
    }
    onScroll(e) {
        const ids = this.props.headings.map(e => e.id);
        const anchors = ids.map(id => ({
            id,
            y_pos: window.document
                .getElementById(id)
                .getBoundingClientRect().y
        }))
            .filter(e => e.y_pos >= 0)
            .sort((a,b) => a.y_pos - b.y_pos);
        this.setState({
            nearest_anchor: anchors[0].id
        })
    }
    render(props) {
        const nested_headings = unflatten(this.props.headings);


        return (
            <Panel ActiveLink={this.state.nearest_anchor}>
                <ToCTitle>Contents</ToCTitle>
                {get_list(nested_headings)}
            </Panel>
        )
    }
} 

export default hot(module)(ToC);