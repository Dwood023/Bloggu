import React from 'react'
import { Router } from 'react-static'
import styled, { injectGlobal } from 'styled-components'
import { hot } from 'react-hot-loader'
import Routes from 'react-static-routes'
import Nav from "./components/Nav"

import {text_color, bg_color} from "./constants";

injectGlobal`
	body {
		@import url('https://fonts.googleapis.com/css?family=IBM+Plex+Mono:200,300,500');
		font-weight: 300;
		font-family: "IBM Plex Mono";
		font-size: 13px;
		margin: auto;

		background-color: ${bg_color};
		color: ${text_color};
		max-width: 700px; /* For center-column layout */
	}
	a {
		text-decoration: underline;
		color: ${text_color};
	}
	.content {
		padding: 1.5rem;
	}
`


const App = () => (
	<Router>
		<div>
			<Nav />
			<div className="content">
				<Routes />
			</div>
		</div>
	</Router>
)

export default hot(module)(App)
